#!/usr/bin/env perl
use Mojolicious::Lite;

get '/' => sub {
  my $c = shift;
  $c->render(template => 'index');
  $c->render(template => 'insert_data_fields');
};

app->start;
__DATA__

# Шаблон начальной страницы
@@ index.html.ep
% layout 'default';
% title 'Welcome';
<h1>Welcome to the Mojolicious real-time web framework!</h1>
# Шаблон страницы ввода данных
@@ insert_data_fields.html.ep
% layout 'default';
% title 'Вставка данных';
<h1>Здесь будут вставляться данные model, oid, cmd, descriptions</h1>

# Шаблон страницы связывания данных
@@ join_data_field.html.ep
% layout 'default';
% title 'Связывание данных';
<h1>Здесь данные model, oid, cmd, descriptions связываются вместе</h1>


@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>
