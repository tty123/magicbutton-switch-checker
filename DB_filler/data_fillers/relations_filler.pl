#!/usr/bin/env perl
#===============================================================================
#
#         FILE: relations_filler.pl
#
#        USAGE: ./relations_filler.pl  
#
#  DESCRIPTION: Скрипт добавляет в таблицу relation соответствие группе моделей,
#  		определённые комманды
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 27.01.2020 14:15:59
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
binmode(STDOUT,':utf8');
#---
use FindBin qw($Bin); # юзы для моего модуля
use lib $Bin;

use DBI();
use DBD::Pg;

use Data::Printer;
use Data::Dumper;
$| = 1;

#----------------------------------------------------------
#  заполнить для подключения к базе
#----------------------------------------------------------
my $host = '172.17.10.2'; # 172.17.10.2 localhost ++++++++++++++ change
my $db_name = "magicbn";
my $port = 5432;
my $user = "caster";
my $pass = "G3puwzHM";

# Заданный список моделей
my %list_vendor_oids = (
			'1.3.6.1.4.1.171.10.61.2' => 'DES-2108     V3.00.27',    
			'1.3.6.1.4.1.171.10.113.8.1' => 'DES-3200-28P/C1 Fast Ethernet Switch', 
			'1.3.6.1.4.1.171.10.113.9.1' => 'DES-3200-52/C1 Fast Ethernet Switch',  
			'1.3.6.1.4.1.171.10.64.1' => 'DES-3526 Fast-Ethernet Switch',           
			'1.3.6.1.4.1.171.10.76.28.2' => 'DGS-1210-28/ME/B1',
			'1.3.6.1.4.1.171.10.76.29.1' => 'DGS-1210-52/ME/A1',
			'1.3.6.1.4.1.171.10.76.29.2' => 'DGS-1210-52/ME/B1',
			'1.3.6.1.4.1.171.10.117.1.3' => 'DGS-3120-24SC Gigabit Ethernet Switch',
			'1.3.6.1.4.1.171.10.117.1.1' => 'DGS-3120-24TC Gigabit Ethernet Switch',
			'1.3.6.1.4.1.171.10.119.2' => 'DGS-3420-28SC Gigabit Ethernet Switch',
			'1.3.6.1.4.1.171.10.70.8' => 'DGS-3627G Gigabit Ethernet Switch',
			'1.3.6.1.4.1.171.10.63.3' => 'D-Link DES-3026 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.63.6' => 'D-Link DES-3028 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.63.11' => 'D-Link DES-3028G Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.63.7' => 'D-Link DES-3028P Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.113.1.3' =>  'D-Link DES-3200-28 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.41.1' => 'D-Link DMC-1000i v1.16',
			'1.3.6.1.4.1.171.10.113.1.1' => 'D-Link DES-3200-10 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.113.1.2' => 'D-Link DES-3200-18 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.113.3.1' => 'D-Link DES-3200-18/Cx Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.113.2.1' => 'D-Link DES-3200-10/Cx Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.76.30.1' => 'D-Link DGS-1210-28P/ME/A1 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.76.30.2' => 'D-Link DGS-1210-28P/ME/B1 Fast Ethernet Switch',
			'1.3.6.1.4.1.17409.1.11' => 'EDFA SNMP AGENT System',
			'1.3.6.1.4.1.5591.1.11' => 'EDFA System',
			'1.3.6.1.4.1.35265.1.21.202.2' => 'ELTEX LTE-2X        ',
			'1.3.6.1.4.1.32285.2.2.1' => 'Enhanced Multimedia Router ',
			'1.3.6.1.4.1.40418.2.2' => 'Fmv_6.X',
			'1.3.6.1.4.1.38295.15.0.0' => 'Made by PBI  Model:DMM-1701IM  SW Version:17IM002B ',
			'1.3.6.1.4.1.30966' => 'mini tesla tp-ats-06a02b-1u ',
			'1.3.6.1.4.1.40418.1.283.0' => 'SNR-S2970-12X Series Software, Version 2.1.1B Build 31318',
			'1.3.6.1.4.1.935' => 'UPS NET Agent II  Phoenixtec Power Co., Ltd. ',
			'1.3.6.1.4.1.144988.1' => 'RouterOS RB1100AHx2 ',
			'1.3.6.1.4.1.40418.7.34' => 'SNR-S2990-16X Device, Compiled Oct 11 10:19:13 2018',
			'1.3.6.1.4.1.40418.7.234' => 'SNR-S2990G-24FX Device, Compiled Jan 09 17:26:07 2017',
			'1.3.6.1.4.1.40418.7.12' => 'SNR-S3650G-48S Device, Compiled May 16 20:59:08',
			'1.3.6.1.4.1.17409.1.7' => 'V1.X 2012/06/16',
			'1.3.6.1.4.1.17409.1.10' => 'V3.X-V4.X 2013/10/11',
);

# Хэш команд - оидов, которые нужно сопоставить данным моделям cmd => oid.id)
my %ins_commands = ('ifOperStatus' => 1, 
		'ifSpeed' => 2,); 

# Хэш моделей  m_oid = > m_id из базы
my %models;

# Хэш команд cmd => oid.id) из базы
my %commands;


#DBI->trace(1);
    my $dbh = DBI->connect("DBI:Pg:dbname=$db_name; host=$host", $user, $pass,{AutoCommit => 1}) or die "Error connecting to database";
    $dbh->do("set application_name = 'perl_model_filler';")
		or  die "My Error  set application name";  #++++++++++++++++new+++++++++


		# загружаем список моделей 
		my $query1 = $dbh->prepare("SELECT m_id, m_oid from swchk.models;");
			  $query1->execute;
		my $m_oid;
		my $m_id;
		$query1->bind_columns(\$m_id,\$m_oid);
			while ($query1->fetch){
				$models{$m_oid} = $m_id;
				}
		$query1->finish;
		# загружаем список комманд
		my $query2 = $dbh->prepare("SELECT cmd_id, cmd from swchk.base_cmd;");
			  $query2->execute;
		my $cmd_id;
		my $cmd;
		$query2->bind_columns(\$cmd_id,\$cmd);
			while ($query2->fetch){
				$commands{$cmd} = $cmd_id;
				}
		$query2->finish;
#------------------------------------------------------------------------------------------
# Основной цикл
#------------------------------------------------------------------------------------------
my @i_models = keys %list_vendor_oids;
	foreach (@i_models){
		my $oid_model = $_; 
		if ($models{$oid_model}){		# проверка модели в списке из базы
			while (my ($i_cmd, $i_oid_id) = each %ins_commands){
				# загружаем из таблицы соответствия, по текущим параметрам, чтоб проверить их существование
				my %relations;
				my $query4 = $dbh->prepare("SELECT r_id, cmd_id, m_id, oid_id from swchk.relations where cmd_id = '$commands{$i_cmd}' and m_id = '$models{$oid_model}' and oid_id = '$i_oid_id';");
					  $query4->execute;
				my $cmd_id;
				my $m_id;
				my $oid_id;
				my $r_id;
				$query4->bind_columns(\$r_id,\$cmd_id,\$m_id,\$oid_id);
					while ($query4->fetch){
						$relations{$i_cmd} = [$r_id, $cmd_id, $m_id, $oid_id];
						}
				$query4->finish;
			if ( !(exists $relations{$i_cmd})){
			$dbh->do("INSERT INTO swchk.relations(cmd_id, m_id, oid_id) VALUES ('$commands{$i_cmd}', '$models{$oid_model}', '$i_oid_id');")
				or die "My error insert into swchk.models";} else {
										print "входные данные уже имеют соответствие r_id = $relations{$i_cmd}[0]\n ";
											}
		}
			}else{
				print "Модель - $oid_model отсутствует в БД. Команды не сопоставленны.\n";
				}
	}
print "Список моделей обработан. Выполнено сопоставление коммандам.\n";
