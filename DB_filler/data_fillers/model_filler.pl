#!/usr/bin/env perl
#===============================================================================
#
#         FILE: model_filler.pl
#
#        USAGE: ./model_filler.pl  
#
#  DESCRIPTION: Скрипт сравнивает модели из хэша %list_vendor_oids с моделями в 
#  		базе данных. Если не обнаруживает совпадений, то добавляет новое 
#  		значение в базу.
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 24.01.2020 09:52:18
#     REVISION: 
#===============================================================================
use strict;
use warnings;
use utf8;
binmode(STDOUT,':utf8');
#---
use FindBin qw($Bin); # юзы для моего модуля
use lib $Bin;

use DBI();
use DBD::Pg;

use Data::Printer;
use Data::Dumper;
$| = 1;

#----------------------------------------------------------
#  заполнить для подключения к базе
#----------------------------------------------------------
my $host = '172.17.10.2'; # 172.17.10.2 localhost ++++++++++++++ change
my $db_name = "magicbn";
my $port = 5432;
my $user = "caster";
my $pass = "G3puwzHM";

my %list_vendor_oids = (
			'1.3.6.1.4.1.171.10.61.2' => 'DES-2108     V3.00.27',    
			'1.3.6.1.4.1.171.10.113.8.1' => 'DES-3200-28P/C1 Fast Ethernet Switch', 
			'1.3.6.1.4.1.171.10.113.9.1' => 'DES-3200-52/C1 Fast Ethernet Switch',  
			'1.3.6.1.4.1.171.10.64.1' => 'DES-3526 Fast-Ethernet Switch',           
			'1.3.6.1.4.1.171.10.76.28.2' => 'DGS-1210-28/ME/B1',
			'1.3.6.1.4.1.171.10.76.29.1' => 'DGS-1210-52/ME/A1',
			'1.3.6.1.4.1.171.10.76.29.2' => 'DGS-1210-52/ME/B1',
			'1.3.6.1.4.1.171.10.117.1.3' => 'DGS-3120-24SC Gigabit Ethernet Switch',
			'1.3.6.1.4.1.171.10.117.1.1' => 'DGS-3120-24TC Gigabit Ethernet Switch',
			'1.3.6.1.4.1.171.10.119.2' => 'DGS-3420-28SC Gigabit Ethernet Switch',
			'1.3.6.1.4.1.171.10.70.8' => 'DGS-3627G Gigabit Ethernet Switch',
			'1.3.6.1.4.1.171.10.63.3' => 'D-Link DES-3026 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.63.6' => 'D-Link DES-3028 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.63.11' => 'D-Link DES-3028G Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.63.7' => 'D-Link DES-3028P Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.113.1.3' =>  'D-Link DES-3200-28 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.41.1' => 'D-Link DMC-1000i v1.16',
			'1.3.6.1.4.1.171.10.113.1.1' => 'D-Link DES-3200-10 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.113.1.2' => 'D-Link DES-3200-18 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.113.3.1' => 'D-Link DES-3200-18/Cx Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.113.2.1' => 'D-Link DES-3200-10/Cx Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.76.30.1' => 'D-Link DGS-1210-28P/ME/A1 Fast Ethernet Switch',
			'1.3.6.1.4.1.171.10.76.30.2' => 'D-Link DGS-1210-28P/ME/B1 Fast Ethernet Switch',
			'1.3.6.1.4.1.17409.1.11' => 'EDFA SNMP AGENT System',
			'1.3.6.1.4.1.5591.1.11' => 'EDFA System',
			'1.3.6.1.4.1.35265.1.21.202.2' => 'ELTEX LTE-2X        ',
			'1.3.6.1.4.1.32285.2.2.1' => 'Enhanced Multimedia Router ',
			'1.3.6.1.4.1.40418.2.2' => 'Fmv_6.X',
			'1.3.6.1.4.1.38295.15.0.0' => 'Made by PBI  Model:DMM-1701IM  SW Version:17IM002B ',
			'1.3.6.1.4.1.30966' => 'mini tesla tp-ats-06a02b-1u ',
			'1.3.6.1.4.1.40418.1.283.0' => 'SNR-S2970-12X Series Software, Version 2.1.1B Build 31318',
			'1.3.6.1.4.1.935' => 'UPS NET Agent II  Phoenixtec Power Co., Ltd. ',
			'1.3.6.1.4.1.144988.1' => 'RouterOS RB1100AHx2 ',
			'1.3.6.1.4.1.40418.7.34' => 'SNR-S2990-16X Device, Compiled Oct 11 10:19:13 2018',
			'1.3.6.1.4.1.40418.7.234' => 'SNR-S2990G-24FX Device, Compiled Jan 09 17:26:07 2017',
			'1.3.6.1.4.1.40418.7.12' => 'SNR-S3650G-48S Device, Compiled May 16 20:59:08',
			'1.3.6.1.4.1.17409.1.7' => 'V1.X 2012/06/16',
			'1.3.6.1.4.1.17409.1.10' => 'V3.X-V4.X 2013/10/11',
);
DBI->trace(1);
    my $dbh = DBI->connect("DBI:Pg:dbname=$db_name; host=$host", $user, $pass,{AutoCommit => 1}) or die "Error connecting to database";
    $dbh->do("set application_name = 'perl_model_filler';")
		or  die "My Error  set application name";  #++++++++++++++++new+++++++++


		# загружаем список таблиц в массив
		my $query = $dbh->prepare("SELECT m_oid from swchk.models;");
			  $query->execute;
		my $m_oid;
		my @b_oid;
		$query->bind_columns(\$m_oid);
			while ($query->fetch){
				push (@b_oid, $m_oid);
				#p %cmd_info;
				}
		$query->finish;
		p @b_oid;
#------------- сверяем модели в базе  со  своим списком -----------------
my @res_oid; #сюда кинем оиды которые не совпадают с теми, что в базе
my @my_oids = keys %list_vendor_oids;
foreach (@my_oids){
	my $use_oid=$_;
	foreach (@b_oid){
		if ($use_oid eq $_){$use_oid = 'dublicat'; last;}
	}
	p $use_oid;
	if (!($use_oid eq 'dublicat')){
	$dbh->do("INSERT INTO swchk.models(m_oid, m_descr) VALUES ('$use_oid', '$list_vendor_oids{$use_oid}');")
		or die "My error insert into swchk.models";
	}
}


