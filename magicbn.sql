--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1 (Ubuntu 11.1-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.1 (Ubuntu 11.1-1.pgdg18.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: swchk; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA swchk;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: base_cmd; Type: TABLE; Schema: swchk; Owner: -
--

CREATE TABLE swchk.base_cmd (
    cmd_id integer NOT NULL,
    cmd character varying NOT NULL,
    cmd_descr character varying
);


--
-- Name: base_cmd_cmd_id_seq; Type: SEQUENCE; Schema: swchk; Owner: -
--

CREATE SEQUENCE swchk.base_cmd_cmd_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: base_cmd_cmd_id_seq; Type: SEQUENCE OWNED BY; Schema: swchk; Owner: -
--

ALTER SEQUENCE swchk.base_cmd_cmd_id_seq OWNED BY swchk.base_cmd.cmd_id;


--
-- Name: models; Type: TABLE; Schema: swchk; Owner: -
--

CREATE TABLE swchk.models (
    m_id integer NOT NULL,
    m_oid character varying NOT NULL,
    m_descr character varying
);


--
-- Name: models_m_id_seq; Type: SEQUENCE; Schema: swchk; Owner: -
--

CREATE SEQUENCE swchk.models_m_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: models_m_id_seq; Type: SEQUENCE OWNED BY; Schema: swchk; Owner: -
--

ALTER SEQUENCE swchk.models_m_id_seq OWNED BY swchk.models.m_id;


--
-- Name: relations; Type: TABLE; Schema: swchk; Owner: -
--

CREATE TABLE swchk.relations (
    r_id integer NOT NULL,
    cmd_id integer NOT NULL,
    m_id integer NOT NULL,
    oid_id integer NOT NULL
);


--
-- Name: models_oids_cmd_id_seq; Type: SEQUENCE; Schema: swchk; Owner: -
--

CREATE SEQUENCE swchk.models_oids_cmd_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: models_oids_cmd_id_seq; Type: SEQUENCE OWNED BY; Schema: swchk; Owner: -
--

ALTER SEQUENCE swchk.models_oids_cmd_id_seq OWNED BY swchk.relations.cmd_id;


--
-- Name: models_oids_m_id_seq; Type: SEQUENCE; Schema: swchk; Owner: -
--

CREATE SEQUENCE swchk.models_oids_m_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: models_oids_m_id_seq; Type: SEQUENCE OWNED BY; Schema: swchk; Owner: -
--

ALTER SEQUENCE swchk.models_oids_m_id_seq OWNED BY swchk.relations.m_id;


--
-- Name: models_oids_mo_oid_seq; Type: SEQUENCE; Schema: swchk; Owner: -
--

CREATE SEQUENCE swchk.models_oids_mo_oid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: models_oids_mo_oid_seq; Type: SEQUENCE OWNED BY; Schema: swchk; Owner: -
--

ALTER SEQUENCE swchk.models_oids_mo_oid_seq OWNED BY swchk.relations.r_id;


--
-- Name: models_oids_oid_id_seq; Type: SEQUENCE; Schema: swchk; Owner: -
--

CREATE SEQUENCE swchk.models_oids_oid_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: models_oids_oid_id_seq; Type: SEQUENCE OWNED BY; Schema: swchk; Owner: -
--

ALTER SEQUENCE swchk.models_oids_oid_id_seq OWNED BY swchk.relations.oid_id;


--
-- Name: oids; Type: TABLE; Schema: swchk; Owner: -
--

CREATE TABLE swchk.oids (
    oid_id integer NOT NULL,
    oid character varying NOT NULL,
    oid_descr character varying,
    oid_end integer,
    var_conv character varying[]
);


--
-- Name: oids_oid_id_seq; Type: SEQUENCE; Schema: swchk; Owner: -
--

CREATE SEQUENCE swchk.oids_oid_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: oids_oid_id_seq; Type: SEQUENCE OWNED BY; Schema: swchk; Owner: -
--

ALTER SEQUENCE swchk.oids_oid_id_seq OWNED BY swchk.oids.oid_id;


--
-- Name: base_cmd cmd_id; Type: DEFAULT; Schema: swchk; Owner: -
--

ALTER TABLE ONLY swchk.base_cmd ALTER COLUMN cmd_id SET DEFAULT nextval('swchk.base_cmd_cmd_id_seq'::regclass);


--
-- Name: models m_id; Type: DEFAULT; Schema: swchk; Owner: -
--

ALTER TABLE ONLY swchk.models ALTER COLUMN m_id SET DEFAULT nextval('swchk.models_m_id_seq'::regclass);


--
-- Name: oids oid_id; Type: DEFAULT; Schema: swchk; Owner: -
--

ALTER TABLE ONLY swchk.oids ALTER COLUMN oid_id SET DEFAULT nextval('swchk.oids_oid_id_seq'::regclass);


--
-- Name: relations r_id; Type: DEFAULT; Schema: swchk; Owner: -
--

ALTER TABLE ONLY swchk.relations ALTER COLUMN r_id SET DEFAULT nextval('swchk.models_oids_mo_oid_seq'::regclass);


--
-- Name: relations cmd_id; Type: DEFAULT; Schema: swchk; Owner: -
--

ALTER TABLE ONLY swchk.relations ALTER COLUMN cmd_id SET DEFAULT nextval('swchk.models_oids_cmd_id_seq'::regclass);


--
-- Name: relations m_id; Type: DEFAULT; Schema: swchk; Owner: -
--

ALTER TABLE ONLY swchk.relations ALTER COLUMN m_id SET DEFAULT nextval('swchk.models_oids_m_id_seq'::regclass);


--
-- Name: relations oid_id; Type: DEFAULT; Schema: swchk; Owner: -
--

ALTER TABLE ONLY swchk.relations ALTER COLUMN oid_id SET DEFAULT nextval('swchk.models_oids_oid_id_seq'::regclass);


--
-- Data for Name: base_cmd; Type: TABLE DATA; Schema: swchk; Owner: -
--

COPY swchk.base_cmd (cmd_id, cmd, cmd_descr) FROM stdin;
2	test2	проверка ещё чего-то
3	test3	узнать адресс
4	test4	узнать поле
5	ifOperStatus	текущее состояние порта
1	ifSpeed	Текущая скорость порта
\.


--
-- Data for Name: models; Type: TABLE DATA; Schema: swchk; Owner: -
--

COPY swchk.models (m_id, m_oid, m_descr) FROM stdin;
1	1.3.6.1.4.1.171.10.61.2	DES-2108     V3.00.27
2	1.3.6.1.4.1.171.10.113.8.1	DES-3200-28P/C1
3	1.3.6.1.4.1.171.10.113.9.1	DES-3200-52/C1
4	1.3.6.1.4.1.171.10.64.1	DES-3526 Fast-Ethernet Switch
5	1.3.6.1.4.1.171.10.76.28.2	DGS-1210-28/ME/B1
6	1.3.6.1.4.1.171.10.76.29.1	DGS-1210-52/ME/A1
7	1.3.6.1.4.1.171.10.76.29.2	DGS-1210-52/ME/B1
8	1.3.6.1.4.1.171.10.117.1.3	DGS-3120-24SC Gigabit Ethernet Switch
9	1.3.6.1.4.1.171.10.117.1.1	DGS-3120-24TC Gigabit Ethernet Switch
10	1.3.6.1.4.1.171.10.113.1.3	D-Link DES-3200-28 Fast Ethernet Switch
\.


--
-- Data for Name: oids; Type: TABLE DATA; Schema: swchk; Owner: -
--

COPY swchk.oids (oid_id, oid, oid_descr, oid_end, var_conv) FROM stdin;
1	1.3.6.1.2.1.2.2.1.8	The current operational state of the interface. The testing(3) state indicates that no operational packets can be passed.	1	{"",up,down,testing}
2	1.3.6.1.2.1.2.2.1.5	An estimate of the interface's current bandwidth in bits per second.  For interfaces which do not vary in bandwidth or for those where no accurate estimation can be made, this object should contain the nominal bandwidth.	1	\N
\.


--
-- Data for Name: relations; Type: TABLE DATA; Schema: swchk; Owner: -
--

COPY swchk.relations (r_id, cmd_id, m_id, oid_id) FROM stdin;
1	5	1	1
4	5	4	1
3	5	3	1
9	5	9	1
7	5	7	1
6	5	6	1
8	5	8	1
2	5	2	1
5	5	5	1
10	1	1	2
11	1	2	2
12	1	3	2
13	1	4	2
14	1	5	2
15	1	6	2
16	1	7	2
17	1	8	2
18	1	9	2
19	5	10	1
20	1	10	2
\.


--
-- Name: base_cmd_cmd_id_seq; Type: SEQUENCE SET; Schema: swchk; Owner: -
--

SELECT pg_catalog.setval('swchk.base_cmd_cmd_id_seq', 5, true);


--
-- Name: models_m_id_seq; Type: SEQUENCE SET; Schema: swchk; Owner: -
--

SELECT pg_catalog.setval('swchk.models_m_id_seq', 9, true);


--
-- Name: models_oids_cmd_id_seq; Type: SEQUENCE SET; Schema: swchk; Owner: -
--

SELECT pg_catalog.setval('swchk.models_oids_cmd_id_seq', 1, false);


--
-- Name: models_oids_m_id_seq; Type: SEQUENCE SET; Schema: swchk; Owner: -
--

SELECT pg_catalog.setval('swchk.models_oids_m_id_seq', 1, false);


--
-- Name: models_oids_mo_oid_seq; Type: SEQUENCE SET; Schema: swchk; Owner: -
--

SELECT pg_catalog.setval('swchk.models_oids_mo_oid_seq', 18, true);


--
-- Name: models_oids_oid_id_seq; Type: SEQUENCE SET; Schema: swchk; Owner: -
--

SELECT pg_catalog.setval('swchk.models_oids_oid_id_seq', 1, false);


--
-- Name: oids_oid_id_seq; Type: SEQUENCE SET; Schema: swchk; Owner: -
--

SELECT pg_catalog.setval('swchk.oids_oid_id_seq', 2, true);


--
-- Name: base_cmd base_cmd_pkey; Type: CONSTRAINT; Schema: swchk; Owner: -
--

ALTER TABLE ONLY swchk.base_cmd
    ADD CONSTRAINT base_cmd_pkey PRIMARY KEY (cmd_id);


--
-- Name: relations models_oids_pkey; Type: CONSTRAINT; Schema: swchk; Owner: -
--

ALTER TABLE ONLY swchk.relations
    ADD CONSTRAINT models_oids_pkey PRIMARY KEY (r_id);


--
-- Name: models models_pkey; Type: CONSTRAINT; Schema: swchk; Owner: -
--

ALTER TABLE ONLY swchk.models
    ADD CONSTRAINT models_pkey PRIMARY KEY (m_id);


--
-- Name: oids oids_pkey; Type: CONSTRAINT; Schema: swchk; Owner: -
--

ALTER TABLE ONLY swchk.oids
    ADD CONSTRAINT oids_pkey PRIMARY KEY (oid_id);


--
-- PostgreSQL database dump complete
--

