#!/usr/bin/env perl
#===============================================================================
#
#         FILE: test_snmp_mcbn.pl
#
#        USAGE: ./test_snmp_mcbn.pl  
#
#  DESCRIPTION: разработка и отладка функции get_switch, получение данных по 
#  		SNMP для Magic Button. Функция принимает ip, набор команд и портов, а
#  		отдаёт JSON с данными, которые приводятся к человеческому виду 
#  		(описания вместо цифр)
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N, 
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 30.12.2019 16:22:49
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
binmode(STDOUT,':utf8');
use JSON;
use Net::SNMP;

use DBI();
use DBD::Pg;

use Data::Printer;
use Data::Dumper;
$| = 1;

#----------------------------------------------------------
#  заполнить для подключения к базе
#----------------------------------------------------------
my $host = '172.17.10.2'; # 172.17.31.13 ++++++++++++++ change
my $db_name = "magicbn";
my $port = 5432;
my $user = "caster";
my $pass = "G3puwzHM";
my $dbh;

my $community = 'public';
#==========================================================
#----- процедура получения vendor_id  -----
sub get_vendor_id {    

my  $ip_  = shift; 
my $sysObjectID = '1.3.6.1.2.1.1.2.0'; # вендор ID
	my ($session, $error) = Net::SNMP->session(
                        -hostname      => $ip_,
                        -community     => $community,
                        -translate     => [-octetstring => 0],
                        -version       => 'snmpv2c',
			-timeout       => 1,
                     ); 
	if (!defined $session) {printf "ERROR: %s.\n", $error; exit 1;}
	my $result = $session->get_request(
			-varbindlist =>[$sysObjectID] ,
	);
	print "+++++++++ get_vendor_id +++++++++";
	p $result;
	return ($result); #возвращаем vendor oid
}

#----- процедура получения хэша команд => оидов -----
sub get_oid_from_db{
my $model_oid = shift;
my %cmd_info; # результат выполнения функции, оид
my @ms = values %{$model_oid};
my $model = shift @ms;

#DBI->trace(1);
$dbh = DBI->connect("DBI:Pg:dbname=$db_name; host=$host; port=$port", $user, $pass,{AutoCommit => 1}) 
		or die "Error connecting to database";
		$dbh->do("set application_name = 'mbn_swchk_api_server';");
		# загружаем список таблиц в массив
		my $query = $dbh->prepare("SELECT DISTINCT t1.cmd_id, t1.cmd, t1.cmd_descr, t4.m_oid, t4.m_descr, t3.oid_id, t3.oid, t3.oid_descr, t3.oid_end, t3.var_conv 
						FROM swchk.base_cmd as t1 
						JOIN swchk.relations as t2 on t1.cmd_id = t2.cmd_id 
						JOIN swchk.oids as t3 on t2.oid_id = t3.oid_id 
						JOIN swchk.models as t4 on t2.m_id = t4.m_id
					       WHERE t4.m_oid = '$model';");
			  $query->execute;
		my ($cmd_id, $cmd, $cmd_descr, $m_oid, $m_descr, $oid_id, $oid, $oid_descr, $oid_end, $var_conv);
		$query->bind_columns(\$cmd_id, \$cmd, \$cmd_descr, \$m_oid, \$m_descr, \$oid_id, \$oid, \$oid_descr, \$oid_end, \$var_conv);
			while ($query->fetch){
				$cmd_info{$cmd} = [$cmd_id, $oid_id, $oid, $oid_end, $var_conv, $oid_descr, $cmd_descr, $m_descr];
				#p %cmd_info;
				}
		$query->finish;
		if (!keys %cmd_info) { 
			$cmd_info{'error'}='sql request get undef resault';
			return \%cmd_info;
		}
	return \%cmd_info;
}
#----- процедура формирования оидов для обработки -----
sub prepare_oids {

my ($base_cmds, $ports, $cmd_info ) = @_; # первые два параметра внешние
my %data_hsh;
my %commands; # хэш команда -> набор оидов
print "++++ \$base_cmds +++++++++++\n";
p $base_cmds;
print "++++ \$ports +++++++++++\n";
p $ports;
print "+++++++++++++++\n";
print "++++ \$cmd_info +++++++++++\n";
p $cmd_info;
print "+++++++++++++++\n";
if ($cmd_info->{'error'}){ print "++++return worked\n"; $commands{'error'}='database'; return \%commands;}
foreach (@{$base_cmds}){
	my $cmdz = $_;
	unless (exists  $cmd_info->{$cmdz}){
		$commands{$cmdz}=["undefined command",];
 	        next;	
	}
	$data_hsh{$cmdz} = $cmd_info -> {$cmdz}; # офильтровываем нужные команды из общего списка
	foreach (@$ports){
		if ( $cmd_info->{$cmdz}->[3] eq '0' ){ %commands=( '$cmdz' => [$cmd_info->{$cmdz}] ) } # если oid_end = 0 - оид не меняем, oid_end = 1 - добавляем порты
				       elsif ( $cmd_info->{$cmdz}->[3] eq '1' ){
						if (!$_=~/\d{5}/){ next;}  #  проверка валидности порта
							
						       my $var = $cmd_info->{$cmdz}->[2].".$_"; # добавляем порт к оиду
							push (@{$commands{$cmdz}}, $var); 
					       
					}
				}
	}
return \%commands; # ссылка на хэш вида "cmd" => @oids 
}
#----- процедура получения SNMP данных согласно массива использованных oid-ов -----
sub get_snmp_req {

my ($ip, $port_oids) = @_; # забираем ссылку на массив оидов
my $res;
print "&&&&&&&&&&&&&&&&&&&&&&&&";
p $port_oids;
p $ip;
# пробиваем все оиды 
	foreach (@{$port_oids}){
	my ($session, $error) = Net::SNMP->session(
                        -hostname      => ${$ip},
                        -community     => $community,
                        -translate     => [-octetstring => 0],
                        -version       => 'snmpv2c',
			-timeout       => 1,
                     ); 
	if (!defined $session) {
	printf "ERROR: %s.\n", $error; 
	exit 1;
	}

	$res = $session->get_request(
			-varbindlist => $port_oids,
	);
	
p $res;

	}
return $res;
} 
#-------------------------------------------

#----- объединяющая, базовая функция -----
sub check_switch {
my ($ip, $ports, $cmd) = @_;

print Dumper $ip;
print Dumper $ports;
print Dumper  $cmd;

#my %error;

if (!(${$ip})=~m%\d{0,3}\.\d{0,3}\.\d{0,3}\.\d{0,3}%){   #  проверка валидности ip адреса
	my %error=('error' => "invalid ip");
	p %error;
	my $js_err = encode_json \%error;
	p $js_err;
	return $js_err;
	}


my @tmparr;
my %full_res;
my %final;
my %fin_hsh;
my $json;

my $vendor = get_vendor_id ($$ip);
print "**** get_vendor_id ****";
p $vendor;
my $cmd_info = get_oid_from_db ($vendor);
p $cmd_info;
my $commands = prepare_oids ($cmd, $ports, $cmd_info );
print "#################\n";
p $commands;
	if ( exists $commands->{'error'} ){return $json = encode_json $commands; }
	while (my ($scmd, @oids) = each (%$commands)){

		my $tmp = get_snmp_req($ip, @oids);
		print "~~~~~~~~~~~~~";
		p $tmp;
		$full_res{$scmd} = $tmp;
		# Сюда вкрячу разбор полётов: замену цифр человеческими символами
			if ( $cmd_info->{$scmd}->[3] eq '0'){   # Портов нет, делаем человеческий вывод
				while (my ($oid, $res) = each %{$tmp}){
				%fin_hsh = ('info' => $res,
					'oid_descr'=> $cmd_info->{$scmd}->[5],
					'cmd_descr'=> $cmd_info->{$scmd}->[6],
						);	
				}
			} elsif ( $cmd_info->{$scmd}->[3] eq '1'){   # Есть порты, делаем человеческий вывод
				while (my ($oid, $res) = each %{$tmp}){
					$oid =~ /\.(\d+)$/;
					print "+++++++++ регулярка ++++++++\n";
					p $1;
					my $res_rename; 
					if ($cmd_info->{$scmd}->[4]){
						$res_rename = $cmd_info->{$scmd}->[4]->[$res]; # человеческий вывод реквеста, если в базе есть трансляция результата
						p $res_rename;
					} else { $res_rename = $res; } 
				$fin_hsh{"port $1"} = $res_rename;	
				}
				$fin_hsh{'oid_descr'} = $cmd_info->{$scmd}->[5],
				$fin_hsh{'cmd_descr'} = $cmd_info->{$scmd}->[6],
				my %data = %fin_hsh;
		$full_res{$scmd} = \%data;
		%fin_hsh=();
		}
	}
		%final=( $$ip => \%full_res);
$json = encode_json \%final;
return $json;
} 

#--------------- СЕКЦИЯ ПРОВЕРКИ ---------------------
my $test = check_switch(\'10.240.16.5400', [1,2,8,], ['ifOperStatus', 'ifSpeed']);

p $test;
