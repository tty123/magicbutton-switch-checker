#!/usr/bin/env perl
#===============================================================================
#
#         FILE: magic_sw_checker.pl
#
#        USAGE: ./magic_sw_checker.pl  
#
#  DESCRIPTION: сервер для сервиса MagicButton
#  		содержит пинговалку свитча, выдает информацию по свитчу 
#  		принимает http запрос и отдаёт в JSON результат поиска.
#               Запускать от имени администратора (нужен пинговалке).
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Tolmachev V.N. (), 
# ORGANIZATION: 
#      VERSION: 1
#      CREATED: 18.12.2019 12:40:39
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
binmode(STDOUT,':utf8');
#---
use FindBin qw($Bin); # юзы для моего модуля
use lib $Bin;
use mbchecker;
#---
#use Net::Ping;
#use Net::SNMP;
use JSON;
use AnyEvent::HTTPD;
#==========================================================
print "Magic button switch checker started!\n";
my $httpd = AnyEvent::HTTPD->new (port => 8468); # здесь указываем порт на котором висит API сервер

$httpd->reg_cb (
   request => sub {
      my ($httpd, $req) = @_;
	my $url=$req->url;
	my $mt=$req->method;
	my $hds=$req->headers;
	my %dt=$req->vars; # хэш значений формы
	my $data;
	if (($url eq '/mswcheck')&&($mt eq 'POST')){
		if ($dt{api} eq 'magic_sw_checker'){
#--------------------------------------------------- проверка функции пинговалки 
			if ($dt{cmd} eq 'ping_switch'){
				my $json_data = mbchecker::ping_switch(\$dt{ip});
				      $req->respond ({ content => ['application/json', $json_data]});
					print "command $dt{cmd} running \n";
					} 
#---------------------------------------------------- забираем данные со свитча
			 elsif ($dt{cmd} eq 'check_switch'){                          
					my @ports = split (/,\s?|\s+/, $dt{ports}); # входной параметр - набор портов
					my @o_cmds = split (/,\s?|\s+/, $dt{oid_cmds}); # входной параметр - базовая команда, аналогичная snmp mib

					my $json_data = mbchecker::check_switch(\$dt{ip}, \@ports, \@o_cmds);  #принимаем ip отдаём массив (ip, vendor_oid)
					#----------------------------------------
					      $req->respond ({ content => ['application/json', $json_data]});
						print "command $dt{cmd} running \n";
				}
#----------------------------------------------------------------
			elsif ($dt{cmd} eq 'set_switch'){
				# здесь будет блок установки значений на snmp устройстве
			}
#----------------------------------------------------------------
			 else{$req->respond ({ content => ['application/json', '{"error":"cmd"}']} ) }; 	
					} else{$req->respond ({ content => ['application/json', '{"error":"api"}']} ) }; 
	} else{$req->respond ({ content => ['application/json','{"error":"url"}']} ) }; 
   },
);

$httpd->run; # запуск сервера 


