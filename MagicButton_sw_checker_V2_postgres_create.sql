CREATE TABLE "base_cmd" (
	"cmd_id" serial NOT NULL,
	"cmd" character varying NOT NULL UNIQUE,
	"cmd_descr" TEXT NOT NULL,
	CONSTRAINT "base_cmd_pk" PRIMARY KEY ("cmd_id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "models" (
	"m_id" serial NOT NULL,
	"m_oid" character varying NOT NULL,
	"m_descr" character varying NOT NULL,
	CONSTRAINT "models_pk" PRIMARY KEY ("m_id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "oids" (
	"oid_id" serial NOT NULL,
	"oid" character varying NOT NULL,
	"oid_descr" TEXT NOT NULL,
	CONSTRAINT "oids_pk" PRIMARY KEY ("oid_id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "models_oids" (
	"mo_oid_id" serial NOT NULL,
	"cmd_id" serial NOT NULL,
	"m_id" serial NOT NULL,
	"oid_id" serial NOT NULL,
	CONSTRAINT "models_oids_pk" PRIMARY KEY ("mo_oid_id")
) WITH (
  OIDS=FALSE
);



ALTER TABLE "base_cmd" ADD CONSTRAINT "base_cmd_fk0" FOREIGN KEY ("cmd_id") REFERENCES "models_oids"("cmd_id");

ALTER TABLE "models" ADD CONSTRAINT "models_fk0" FOREIGN KEY ("m_id") REFERENCES "models_oids"("m_id");

ALTER TABLE "oids" ADD CONSTRAINT "oids_fk0" FOREIGN KEY ("oid_id") REFERENCES "models_oids"("oid_id");


